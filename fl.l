%{
#include "calc.tab.h"
%}

digit           [0-9]
nonzero         [1-9]
octaldigit      [0-7]
hexdigit        [0-9a-fA-F]
exp             [Ee][+-]?{digit}+
frac            [.]?{digit}+
hexfrac         [.]?{hexdigit}+
hexfloat        0[xX]{hexdigit}+{hexfrac}?[pP][+-]?{digit}+
decfloat        {frac}?{exp}? | {digit}+{exp}?
float           {decfloat} | {hexfloat}
octalconst      0{octaldigit}+
decimalconst    {nonzero}{digit}* | 0
hexadecimalconst    0[xX]{hexdigit}+
ws              [ \t\r]+

%%

{decimalconst}          { yylval.int_val = atoi(yytext); return INTEGER; }
{float}                 { yylval.float_val = atof(yytext); return FLOAT; }
{ADD}                   { return ADD; }
{SUB}                   { return SUB; }
{MUL}                   { return MUL; }
{DIV}                   { return DIV; }
{LP}                    { return LP; }
{RP}                    { return RP; }
{EOL}                   { return EOL; }
{ws}                    ; /* skip whitespace */
.                       { yyerror("Invalid character"); }

%%

int yywrap(void) {
    return 1;
}
