%{
#include <stdio.h>
%}

%token INTEGER FLOAT
%token ADD SUB MUL DIV
%token LP RP EOL

%%

calclist: /* empty */
        | calclist exp EOL { printf("=%0.3f\n", $2); }
        ;

exp: term
    | exp ADD term { $$ = $1 + $3; }
    | exp SUB term { $$ = $1 - $3; }
    ;

term: factor
    | term MUL factor { $$ = $1 * $3; }
    | term DIV factor { if ($3 == 0) yyerror("division by zero"); else $$ = $1 / $3; }
    ;

factor: INTEGER { $$ = $1; }
        | FLOAT { $$ = $1; }
        | LP exp RP { $$ = $2; }
        ;

%%

void yyerror(const char *s) {
    printf("%s.\n", s);
}

int main() {
    yyparse();
    return 0;
}
